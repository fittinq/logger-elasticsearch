# PSR Logger to connect with ElasticSearch
This library gives a framework to log to ElasticSearch. It has a few concepts that might look weird at first glance.

## Connect to ElasticSearch
First we connect to elastic search with the client builder. You can set this up in your service.yml as follows
```yaml

Fittinq\Logger\Elastic\ClientBuilder:
    arguments:
        - '%env(ELASTIC_LOGGER_HOST)%'
```
## Indices
The ElasticSearch logger must write its message to a given index. Because you might want to decide you want an index by
day, or an index by the alphabet we introduced the IndexResolver.

To get you going we added a IndexResolver class that simply names the index by any name that you pass to its 
constructor. We provided a second resolver called DateIndex which suffixes the given name with the date in yyyymmdd 
format.

These can be added to your project by wiring them as follows:
```yaml
Fittinq\Logger\Index\IndexResolver:
    arguments:
        - 'my_index'
```
or
```yaml

Fittinq\Logger\Index\DateIndex:
    arguments:
        - 'prefix_%env(APP_ENV)%'
        - '_Ymd'
        
```
## Context
When you log a message, you probably might want to add some context. Such as which service initiated the log message.
You can provide context where you log the message since PSR-3 supports this. However, this might be inconvenient. You 
might not have the right data at hand. For instance, you don't want to have every class that logs a message know what 
the service they're in is called. In fact, you might not even be able to add that to a class if it comes from another 
library.

We use the ContextResolver for this. You can use this to set up logic that is added to the context every time you log 
something.
```yaml
Fittinq\Logger\Context\ContextResolver:
    arguments:
        - { arg1: 'myvalue', arg2: 'myvalue2' }
```
## Set up the actual logger
Then we tie all pieces together:
```yaml
   Fittinq\Logger\Logger\ElasticSearchLogger:
        arguments:
            - '@Fittinq\Logger\Elastic\ClientBuilder'
            - '@Fittinq\Logger\Index\DateIndex'
            - '@Fittinq\Logger\Context\ContextResolver'
```

