<?php declare(strict_types=1);

namespace Fittinq\Logger\Context;

class ContextResolver
{
    private array $baseContext;

    public function __construct(array $baseContext = [])
    {
        $this->baseContext = $baseContext;
    }

    public function getContext(array $context): array
    {
        return array_merge($this->baseContext, $context);
    }
}
