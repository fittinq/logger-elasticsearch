<?php declare(strict_types=1);

namespace Fittinq\Logger\Elastic;

use Elasticsearch\Client;

class ClientBuilder
{
    private Client $client;

    public function __construct(string $host)
    {
        $clientBuilder = \Elasticsearch\ClientBuilder::create();
        $clientBuilder->setHosts([$host]);
        $this->client = $clientBuilder->build();
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
