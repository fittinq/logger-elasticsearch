<?php declare(strict_types=1);

namespace Fittinq\Logger\Exception;

class UndefinedLoglevelException extends \RuntimeException
{
}
