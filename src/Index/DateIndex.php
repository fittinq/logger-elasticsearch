<?php declare(strict_types=1);

namespace Fittinq\Logger\Index;

class DateIndex extends IndexResolver
{
    private string $format;

    public function __construct(string $index, string $format = '-Ymd')
    {
        parent::__construct($index);
        $this->format = $format;
    }

    public function getIndex(): string
    {
        return parent::getIndex() . date($this->format);
    }
}
