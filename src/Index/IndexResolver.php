<?php declare(strict_types=1);

namespace Fittinq\Logger\Index;

class IndexResolver
{
    private string $index;

    public function __construct(string $index = 'no_name')
    {
        $this->index = $index;
    }

    public function getIndex(): string
    {
        return $this->index;
    }
}
