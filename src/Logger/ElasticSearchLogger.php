<?php declare(strict_types=1);

namespace Fittinq\Logger\Logger;

use Elasticsearch\Client;
use Fittinq\Logger\Context\ContextResolver;
use Fittinq\Logger\Elastic\ClientBuilder;
use Fittinq\Logger\Exception\UndefinedLoglevelException;
use Fittinq\Logger\Index\IndexResolver;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

class ElasticSearchLogger extends AbstractLogger
{
    private Client $client;
    private IndexResolver $indexResolver;
    private ContextResolver $contextResolver;

    public function __construct(ClientBuilder $clientBuilder, IndexResolver $indexResolver, ContextResolver $contextResolver)
    {
        $this->client = $clientBuilder->getClient();
        $this->indexResolver = $indexResolver;
        $this->contextResolver = $contextResolver;
    }

    public function log($level, $message, array $context = []): void
    {
        $this->throwExceptionIfUndefinedLogLevel($level);
        $this->client->index($this->createDocument($level, $message, $context));
    }

    private function throwExceptionIfUndefinedLogLevel(mixed $level): void
    {
        if (!in_array($level, [
            LogLevel::EMERGENCY,
            LogLevel::ALERT,
            LogLevel::CRITICAL,
            LogLevel::ERROR,
            LogLevel::WARNING,
            LogLevel::NOTICE,
            LogLevel::INFO,
            LogLevel::DEBUG
        ])) {
            throw new UndefinedLoglevelException("Can not log to an undefined level");
        }
    }

    private function createDocument(string $level, string $message, array $context): array
    {
        return [
            'index' => $this->indexResolver->getIndex(),
            'body' => [
                'level' => $level,
                'createdAt' => round(microtime(true) * 1000),
                'message' => $message,
                'context' => $this->contextResolver->getContext($context)
            ]
        ];
    }
}