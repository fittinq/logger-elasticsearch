<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\ElasticSearch;

use Elasticsearch\Client;
use Fittinq\Logger\Elastic\ClientBuilder;

class ClientBuilderMock extends ClientBuilder
{
    private Client $client;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
