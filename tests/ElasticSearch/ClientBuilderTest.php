<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\ElasticSearch;

use Fittinq\Logger\Elastic\ClientBuilder;
use PHPUnit\Framework\TestCase;

class ClientBuilderTest extends TestCase
{
    public function test_createClientWithSingleHost_expectClientToHaveConnectionWithHost()
    {
        $url = 'https://test.elastic.fittinq.com:1234';

        $clientBuilder = new ClientBuilder($url);
        $client = $clientBuilder->getClient();

        $this->assertEquals(parse_url($url, PHP_URL_SCHEME), $client->transport->getConnection()->getTransportSchema());
        $this->assertEquals(parse_url($url, PHP_URL_HOST), $client->transport->getConnection()->getHost());
        $this->assertEquals(parse_url($url, PHP_URL_PORT), $client->transport->getConnection()->getPort());
    }
}
