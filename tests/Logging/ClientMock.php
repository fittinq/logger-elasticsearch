<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Elasticsearch\Client;
use Error;
use Exception;
use PHPUnit\Framework\Assert;
use Throwable;

class ClientMock extends Client
{
    private ?Throwable $throwable = null;
    private array $actualParams = [];

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        // We don't call the parent constructor here, since we want to completely mock this class out.
    }

    public function setUpToThrowException(string $exceptionMessage = '')
    {
        $this->throwable = new Exception($exceptionMessage);
    }

    public function setUpToThrowError(string $errorMessage)
    {
        $this->throwable = new Error($errorMessage);
    }

    /**
     * @throws Throwable
     */
    public function index(array $params = [])
    {
        if ($this->throwable) {
            throw $this->throwable;
        }

        $this->actualParams = $params;
    }

    public function expectDocumentToBeSaved(string $logLevel, string $message, array $context = [])
    {
        $params = [
            'index' => 'no_name',
            'body'  => [
                'level' => $logLevel,
                'message' => $message,
                'createdAt' => time(),
                'context' => $context
            ]
        ];

        Assert::assertEquals($params, $this->actualParams);
    }

    public function expectIndexToBeEnsured(string $index)
    {
        Assert::assertEquals($index, $this->actualParams['index']);
    }

    public function expectContextToBePartOfDocument(array $context)
    {
        Assert::assertEquals($context, $this->actualParams['body']['context']);
    }
}
