<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Fittinq\Logger\Context\ContextResolver;
use Fittinq\Logger\Logger\ElasticSearchLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

class ContextTest extends TestCase
{
    private LoggingConfiguration $configuration;
    private ElasticSearchLogger $logger;
    private ClientMock $clientMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new LoggingConfiguration();
        $this->logger = $this->configuration->configureSimple();
        $this->clientMock = $this->configuration->getClientMock();
    }

    public function test_addContextViaLogMethod_expectContextToBePartOfDocument()
    {
        $context = ["my_context" => 123];
        $this->logger->log(LogLevel::INFO, 'Log this line', $context);
        $this->clientMock->expectContextToBePartOfDocument($context);
    }

    /**
     * A base context is a context that is set at creation time of the elastic search logger and can contain contexts
     * that don't change value during the execution of the process.
     */
    public function test_addBaseContextViaContextResolverConstructor_expectContextToBePartOfDocument()
    {
        $baseContext = ["my_context" => 123];
        $contextResolver = new ContextResolver($baseContext);

        $this->logger = $this->configuration->configureWithContextResolver($contextResolver);
        $this->logger->log(LogLevel::INFO, 'Log this line', []);

        $this->clientMock->expectContextToBePartOfDocument($baseContext);
    }

    public function test_createBaseContextAndOverwriteValueWithContext_expectContextToOverwriteBaseValues()
    {
        $baseContext = ["my_context" => 123, "food" => 'banana'];
        $context = ["my_context" => 456, 'date' => 'today', 'weather' => 'rainy'];
        $contextResolver = new ContextResolver($baseContext);

        $this->logger = $this->configuration->configureWithContextResolver($contextResolver);
        $this->logger->log(LogLevel::INFO, 'Log this line', $context);

        $this->clientMock->expectContextToBePartOfDocument([
            "my_context" => 456,
            'date' => 'today',
            'weather' => 'rainy',
            "food" => 'banana'
        ]);
    }
}
