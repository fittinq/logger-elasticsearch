<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Error;
use Exception;
use Fittinq\Logger\Logger\ElasticSearchLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

class ExceptionTest extends TestCase
{
    private ElasticSearchLogger $logger;
    private ClientMock $clientMock;

    protected function setUp(): void
    {
        parent::setUp();

        $configuration = new LoggingConfiguration();
        $this->logger = $configuration->configureSimple();
        $this->clientMock = $configuration->getClientMock();
    }

    public function test_writeInfoLogWhenClientThrowsExceptions_expectException()
    {
        $exceptionMessage = 'Something went wrong';

        $this->clientMock->setUpToThrowException($exceptionMessage);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->logger->log(LogLevel::INFO, 'Log this line');
    }

    public function test_writeInfoLogWhenClientThrowsError_expectError()
    {
        $errorMessage = 'Something went wrong';

        $this->clientMock->setUpToThrowError($errorMessage);
        $this->expectException(Error::class);
        $this->expectExceptionMessage($errorMessage);

        $this->logger->log(LogLevel::INFO, 'Log this line');
    }
}