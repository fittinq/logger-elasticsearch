<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Fittinq\Logger\Index\DateIndex;
use Fittinq\Logger\Index\IndexResolver;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

class IndexTest extends TestCase
{
    private LoggingConfiguration $configuration;
    private ClientMock $clientMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new LoggingConfiguration();
        $this->clientMock = $this->configuration->getClientMock();
    }

    public function test_writeToASimpleIndex_expectIndexToBeEnsuredWithGivenValue()
    {
        $index = 'my_index';

        $logger = $this->configuration->configureWithIndexResolver(new IndexResolver($index));
        $logger->log(LogLevel::INFO, 'Log this line');

        $this->clientMock->expectIndexToBeEnsured($index);
    }

    public function test_writeToADateIndex_expectIndexToBeEnsuredWithTheDateOfTodayPrefixedWithGivenValue()
    {
        $prefix = 'my_prefix';
        $dateOfToday = date('Ymd');
        $index = $prefix . '-' . $dateOfToday;

        $logger = $this->configuration->configureWithIndexResolver(new DateIndex($prefix));
        $logger->log(LogLevel::INFO, 'Log this line');

        $this->clientMock->expectIndexToBeEnsured($index);
    }
}