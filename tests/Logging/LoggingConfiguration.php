<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Fittinq\Logger\Context\ContextResolver;
use Fittinq\Logger\Logger\ElasticSearchLogger;
use Fittinq\Logger\Index\IndexResolver;
use Test\Fittinq\Logger\ElasticSearch\ClientBuilderMock;

class LoggingConfiguration
{
    private ClientMock $clientMock;
    private ClientBuilderMock $clientBuilderMock;

    public function __construct()
    {
        $this->clientMock = new ClientMock();
        $this->clientBuilderMock = new ClientBuilderMock($this->clientMock);
    }

    public function configureSimple(): ElasticSearchLogger
    {
        return $this->configureElasticSearchLogger(new IndexResolver(), new ContextResolver());
    }

    public function configureWithIndexResolver(IndexResolver $indexResolver): ElasticSearchLogger
    {
        return $this->configureElasticSearchLogger($indexResolver, new ContextResolver());
    }

    public function configureWithContextResolver(ContextResolver $contextResolver): ElasticSearchLogger
    {
        return $this->configureElasticSearchLogger(new IndexResolver(), $contextResolver);
    }

    public function configureElasticSearchLogger(IndexResolver $indexResolver, ContextResolver $contextResolver): ElasticSearchLogger
    {
        return new ElasticSearchLogger($this->clientBuilderMock, $indexResolver, $contextResolver);
    }

    public function getClientMock(): ClientMock
    {
        return $this->clientMock;
    }
}