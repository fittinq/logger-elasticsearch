<?php declare(strict_types=1);

namespace Test\Fittinq\Logger\Logging;

use Fittinq\Logger\Exception\UndefinedLoglevelException;
use Fittinq\Logger\Logger\ElasticSearchLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class LoggingTest extends TestCase
{
    private ClientMock $elasticSearchClient;
    private ElasticSearchLogger $elasticSearchLogger;

    protected function setUp(): void
    {
        parent::setUp();

        $loggingModule = new LoggingConfiguration();
        $this->elasticSearchClient = $loggingModule->getClientMock();
        $this->elasticSearchLogger = $loggingModule->configureSimple();
    }

    public function test_expectLoggerToBeAPSRLoggerInterface()
    {
        $this->assertInstanceOf(LoggerInterface::class, $this->elasticSearchLogger);
    }

    public function test_writeUndefinedLogLevel_expectUndefinedLoglevelException()
    {
        $this->expectException(UndefinedLoglevelException::class);
        $this->expectExceptionMessage("Can not log to an undefined level");
        $this->elasticSearchLogger->log('i_do_not_exist', 'Log this line');
    }

    /**
     * @dataProvider getLogLevels()
     */
    public function test_writeArbitraryLogLevel_expectIndexAndDocumentToBeSavedToElasticSearch(string $logLevel)
    {
        $message = 'Log this line';
        $this->elasticSearchLogger->log($logLevel, $message);
        $this->elasticSearchClient->expectDocumentToBeSaved($logLevel, $message);
    }

    /**
     * @dataProvider getArbitraryMessages()
     */
    public function test_writeArbitraryMessage_expectIndexAndDocumentToBeSavedToElasticSearch(string $message)
    {
        $logLevel = LogLevel::INFO;
        $this->elasticSearchLogger->log($logLevel, $message);
        $this->elasticSearchClient->expectDocumentToBeSaved($logLevel, $message);
    }

    public function getLogLevels(): array
    {
        return [
            [LogLevel::EMERGENCY],
            [LogLevel::ALERT],
            [LogLevel::CRITICAL],
            [LogLevel::ERROR],
            [LogLevel::WARNING],
            [LogLevel::NOTICE],
            [LogLevel::INFO],
            [LogLevel::DEBUG],
        ];
    }

    public function getArbitraryMessages(): array
    {
        return [
            ['Hello world!'],
            ['We were somewhere around Barstow'],
            ['on the edge of the dessert'],
            ['When the ...'],
        ];
    }
}
